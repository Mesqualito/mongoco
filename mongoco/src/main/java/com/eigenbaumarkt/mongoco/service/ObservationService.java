package com.eigenbaumarkt.mongoco.service;

import com.eigenbaumarkt.mongoco.service.dto.ObservationDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing Observation.
 */
public interface ObservationService {

    /**
     * Save a observation.
     *
     * @param observationDTO the entity to save
     * @return the persisted entity
     */
    ObservationDTO save(ObservationDTO observationDTO);

    /**
     * Get all the observations.
     *
     * @return the list of entities
     */
    List<ObservationDTO> findAll();


    /**
     * Get the "id" observation.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<ObservationDTO> findOne(String id);

    /**
     * Delete the "id" observation.
     *
     * @param id the id of the entity
     */
    void delete(String id);

    /**
     * Search for the observation corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @return the list of entities
     */
    List<ObservationDTO> search(String query);
}
