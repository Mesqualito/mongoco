export interface IArticle {
    id?: string;
    intID?: number;
    languageID?: number;
    connectionID?: number;
    observationID?: number;
}

export class Article implements IArticle {
    constructor(
        public id?: string,
        public intID?: number,
        public languageID?: number,
        public connectionID?: number,
        public observationID?: number
    ) {}
}
