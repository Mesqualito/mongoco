package com.eigenbaumarkt.mongoco.service.mapper;

import com.eigenbaumarkt.mongoco.domain.*;
import com.eigenbaumarkt.mongoco.service.dto.ConnectionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Connection and its DTO ConnectionDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ConnectionMapper extends EntityMapper<ConnectionDTO, Connection> {



    default Connection fromId(String id) {
        if (id == null) {
            return null;
        }
        Connection connection = new Connection();
        connection.setId(id);
        return connection;
    }
}
