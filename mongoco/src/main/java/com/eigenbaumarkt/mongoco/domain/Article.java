package com.eigenbaumarkt.mongoco.domain;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Article.
 */
@Document(collection = "article")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "article")
public class Article implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    private String id;

    @Field("int_id")
    private Long intID;

    @Field("language_id")
    private Long languageID;

    @Field("connection_id")
    private Long connectionID;

    @Field("observation_id")
    private Long observationID;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getIntID() {
        return intID;
    }

    public Article intID(Long intID) {
        this.intID = intID;
        return this;
    }

    public void setIntID(Long intID) {
        this.intID = intID;
    }

    public Long getLanguageID() {
        return languageID;
    }

    public Article languageID(Long languageID) {
        this.languageID = languageID;
        return this;
    }

    public void setLanguageID(Long languageID) {
        this.languageID = languageID;
    }

    public Long getConnectionID() {
        return connectionID;
    }

    public Article connectionID(Long connectionID) {
        this.connectionID = connectionID;
        return this;
    }

    public void setConnectionID(Long connectionID) {
        this.connectionID = connectionID;
    }

    public Long getObservationID() {
        return observationID;
    }

    public Article observationID(Long observationID) {
        this.observationID = observationID;
        return this;
    }

    public void setObservationID(Long observationID) {
        this.observationID = observationID;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Article article = (Article) o;
        if (article.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), article.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Article{" +
            "id=" + getId() +
            ", intID=" + getIntID() +
            ", languageID=" + getLanguageID() +
            ", connectionID=" + getConnectionID() +
            ", observationID=" + getObservationID() +
            "}";
    }
}
