import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IObservation } from 'app/shared/model/mongoco/observation.model';

type EntityResponseType = HttpResponse<IObservation>;
type EntityArrayResponseType = HttpResponse<IObservation[]>;

@Injectable({ providedIn: 'root' })
export class ObservationService {
    public resourceUrl = SERVER_API_URL + 'mongoco/api/observations';
    public resourceSearchUrl = SERVER_API_URL + 'mongoco/api/_search/observations';

    constructor(protected http: HttpClient) {}

    create(observation: IObservation): Observable<EntityResponseType> {
        return this.http.post<IObservation>(this.resourceUrl, observation, { observe: 'response' });
    }

    update(observation: IObservation): Observable<EntityResponseType> {
        return this.http.put<IObservation>(this.resourceUrl, observation, { observe: 'response' });
    }

    find(id: string): Observable<EntityResponseType> {
        return this.http.get<IObservation>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IObservation[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    search(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IObservation[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
    }
}
