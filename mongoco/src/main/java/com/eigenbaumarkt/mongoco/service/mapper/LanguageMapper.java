package com.eigenbaumarkt.mongoco.service.mapper;

import com.eigenbaumarkt.mongoco.domain.*;
import com.eigenbaumarkt.mongoco.service.dto.LanguageDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Language and its DTO LanguageDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface LanguageMapper extends EntityMapper<LanguageDTO, Language> {



    default Language fromId(String id) {
        if (id == null) {
            return null;
        }
        Language language = new Language();
        language.setId(id);
        return language;
    }
}
