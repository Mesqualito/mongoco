package com.eigenbaumarkt.mongoco.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of ObservationSearchRepository to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class ObservationSearchRepositoryMockConfiguration {

    @MockBean
    private ObservationSearchRepository mockObservationSearchRepository;

}
