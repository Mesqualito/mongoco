export interface IConnection {
    id?: string;
    intID?: number;
    articleID?: number;
    languageID?: number;
    observationID?: number;
    term?: string;
}

export class Connection implements IConnection {
    constructor(
        public id?: string,
        public intID?: number,
        public articleID?: number,
        public languageID?: number,
        public observationID?: number,
        public term?: string
    ) {}
}
