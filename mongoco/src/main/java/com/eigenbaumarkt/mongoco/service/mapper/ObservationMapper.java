package com.eigenbaumarkt.mongoco.service.mapper;

import com.eigenbaumarkt.mongoco.domain.*;
import com.eigenbaumarkt.mongoco.service.dto.ObservationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Observation and its DTO ObservationDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ObservationMapper extends EntityMapper<ObservationDTO, Observation> {



    default Observation fromId(String id) {
        if (id == null) {
            return null;
        }
        Observation observation = new Observation();
        observation.setId(id);
        return observation;
    }
}
