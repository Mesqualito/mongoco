import { element, by, ElementFinder } from 'protractor';

export class ConnectionComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-connection div table .btn-danger'));
    title = element.all(by.css('jhi-connection div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class ConnectionUpdatePage {
    pageTitle = element(by.id('jhi-connection-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    intIDInput = element(by.id('field_intID'));
    articleIDInput = element(by.id('field_articleID'));
    languageIDInput = element(by.id('field_languageID'));
    observationIDInput = element(by.id('field_observationID'));
    termInput = element(by.id('field_term'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setIntIDInput(intID) {
        await this.intIDInput.sendKeys(intID);
    }

    async getIntIDInput() {
        return this.intIDInput.getAttribute('value');
    }

    async setArticleIDInput(articleID) {
        await this.articleIDInput.sendKeys(articleID);
    }

    async getArticleIDInput() {
        return this.articleIDInput.getAttribute('value');
    }

    async setLanguageIDInput(languageID) {
        await this.languageIDInput.sendKeys(languageID);
    }

    async getLanguageIDInput() {
        return this.languageIDInput.getAttribute('value');
    }

    async setObservationIDInput(observationID) {
        await this.observationIDInput.sendKeys(observationID);
    }

    async getObservationIDInput() {
        return this.observationIDInput.getAttribute('value');
    }

    async setTermInput(term) {
        await this.termInput.sendKeys(term);
    }

    async getTermInput() {
        return this.termInput.getAttribute('value');
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class ConnectionDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-connection-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-connection'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
