/**
 * Spring Data Elasticsearch repositories.
 */
package com.eigenbaumarkt.mongoco.repository.search;
