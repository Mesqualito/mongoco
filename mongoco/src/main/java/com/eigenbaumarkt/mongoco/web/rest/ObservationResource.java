package com.eigenbaumarkt.mongoco.web.rest;
import com.eigenbaumarkt.mongoco.service.ObservationService;
import com.eigenbaumarkt.mongoco.web.rest.errors.BadRequestAlertException;
import com.eigenbaumarkt.mongoco.web.rest.util.HeaderUtil;
import com.eigenbaumarkt.mongoco.service.dto.ObservationDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Observation.
 */
@RestController
@RequestMapping("/api")
public class ObservationResource {

    private final Logger log = LoggerFactory.getLogger(ObservationResource.class);

    private static final String ENTITY_NAME = "mongocoObservation";

    private final ObservationService observationService;

    public ObservationResource(ObservationService observationService) {
        this.observationService = observationService;
    }

    /**
     * POST  /observations : Create a new observation.
     *
     * @param observationDTO the observationDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new observationDTO, or with status 400 (Bad Request) if the observation has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/observations")
    public ResponseEntity<ObservationDTO> createObservation(@Valid @RequestBody ObservationDTO observationDTO) throws URISyntaxException {
        log.debug("REST request to save Observation : {}", observationDTO);
        if (observationDTO.getId() != null) {
            throw new BadRequestAlertException("A new observation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ObservationDTO result = observationService.save(observationDTO);
        return ResponseEntity.created(new URI("/api/observations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /observations : Updates an existing observation.
     *
     * @param observationDTO the observationDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated observationDTO,
     * or with status 400 (Bad Request) if the observationDTO is not valid,
     * or with status 500 (Internal Server Error) if the observationDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/observations")
    public ResponseEntity<ObservationDTO> updateObservation(@Valid @RequestBody ObservationDTO observationDTO) throws URISyntaxException {
        log.debug("REST request to update Observation : {}", observationDTO);
        if (observationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ObservationDTO result = observationService.save(observationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, observationDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /observations : get all the observations.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of observations in body
     */
    @GetMapping("/observations")
    public List<ObservationDTO> getAllObservations() {
        log.debug("REST request to get all Observations");
        return observationService.findAll();
    }

    /**
     * GET  /observations/:id : get the "id" observation.
     *
     * @param id the id of the observationDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the observationDTO, or with status 404 (Not Found)
     */
    @GetMapping("/observations/{id}")
    public ResponseEntity<ObservationDTO> getObservation(@PathVariable String id) {
        log.debug("REST request to get Observation : {}", id);
        Optional<ObservationDTO> observationDTO = observationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(observationDTO);
    }

    /**
     * DELETE  /observations/:id : delete the "id" observation.
     *
     * @param id the id of the observationDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/observations/{id}")
    public ResponseEntity<Void> deleteObservation(@PathVariable String id) {
        log.debug("REST request to delete Observation : {}", id);
        observationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * SEARCH  /_search/observations?query=:query : search for the observation corresponding
     * to the query.
     *
     * @param query the query of the observation search
     * @return the result of the search
     */
    @GetMapping("/_search/observations")
    public List<ObservationDTO> searchObservations(@RequestParam String query) {
        log.debug("REST request to search Observations for query {}", query);
        return observationService.search(query);
    }

}
