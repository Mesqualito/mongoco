package com.eigenbaumarkt.mongoco.service;

import com.eigenbaumarkt.mongoco.service.dto.ConnectionDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing Connection.
 */
public interface ConnectionService {

    /**
     * Save a connection.
     *
     * @param connectionDTO the entity to save
     * @return the persisted entity
     */
    ConnectionDTO save(ConnectionDTO connectionDTO);

    /**
     * Get all the connections.
     *
     * @return the list of entities
     */
    List<ConnectionDTO> findAll();


    /**
     * Get the "id" connection.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<ConnectionDTO> findOne(String id);

    /**
     * Delete the "id" connection.
     *
     * @param id the id of the entity
     */
    void delete(String id);

    /**
     * Search for the connection corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @return the list of entities
     */
    List<ConnectionDTO> search(String query);
}
