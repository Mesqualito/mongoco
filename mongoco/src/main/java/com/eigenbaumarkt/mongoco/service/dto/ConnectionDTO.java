package com.eigenbaumarkt.mongoco.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Connection entity.
 */
public class ConnectionDTO implements Serializable {

    private String id;

    private Long intID;

    private Long articleID;

    private Long languageID;

    private Long observationID;

    @NotNull
    private String term;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getIntID() {
        return intID;
    }

    public void setIntID(Long intID) {
        this.intID = intID;
    }

    public Long getArticleID() {
        return articleID;
    }

    public void setArticleID(Long articleID) {
        this.articleID = articleID;
    }

    public Long getLanguageID() {
        return languageID;
    }

    public void setLanguageID(Long languageID) {
        this.languageID = languageID;
    }

    public Long getObservationID() {
        return observationID;
    }

    public void setObservationID(Long observationID) {
        this.observationID = observationID;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ConnectionDTO connectionDTO = (ConnectionDTO) o;
        if (connectionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), connectionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ConnectionDTO{" +
            "id=" + getId() +
            ", intID=" + getIntID() +
            ", articleID=" + getArticleID() +
            ", languageID=" + getLanguageID() +
            ", observationID=" + getObservationID() +
            ", term='" + getTerm() + "'" +
            "}";
    }
}
