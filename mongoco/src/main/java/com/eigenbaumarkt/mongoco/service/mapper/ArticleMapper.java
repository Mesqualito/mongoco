package com.eigenbaumarkt.mongoco.service.mapper;

import com.eigenbaumarkt.mongoco.domain.*;
import com.eigenbaumarkt.mongoco.service.dto.ArticleDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Article and its DTO ArticleDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ArticleMapper extends EntityMapper<ArticleDTO, Article> {



    default Article fromId(String id) {
        if (id == null) {
            return null;
        }
        Article article = new Article();
        article.setId(id);
        return article;
    }
}
