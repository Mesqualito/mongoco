import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IConnection } from 'app/shared/model/mongoco/connection.model';
import { AccountService } from 'app/core';
import { ConnectionService } from './connection.service';

@Component({
    selector: 'jhi-connection',
    templateUrl: './connection.component.html'
})
export class ConnectionComponent implements OnInit, OnDestroy {
    connections: IConnection[];
    currentAccount: any;
    eventSubscriber: Subscription;
    currentSearch: string;

    constructor(
        protected connectionService: ConnectionService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected activatedRoute: ActivatedRoute,
        protected accountService: AccountService
    ) {
        this.currentSearch =
            this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search']
                ? this.activatedRoute.snapshot.params['search']
                : '';
    }

    loadAll() {
        if (this.currentSearch) {
            this.connectionService
                .search({
                    query: this.currentSearch
                })
                .pipe(
                    filter((res: HttpResponse<IConnection[]>) => res.ok),
                    map((res: HttpResponse<IConnection[]>) => res.body)
                )
                .subscribe((res: IConnection[]) => (this.connections = res), (res: HttpErrorResponse) => this.onError(res.message));
            return;
        }
        this.connectionService
            .query()
            .pipe(
                filter((res: HttpResponse<IConnection[]>) => res.ok),
                map((res: HttpResponse<IConnection[]>) => res.body)
            )
            .subscribe(
                (res: IConnection[]) => {
                    this.connections = res;
                    this.currentSearch = '';
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.currentSearch = query;
        this.loadAll();
    }

    clear() {
        this.currentSearch = '';
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInConnections();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IConnection) {
        return item.id;
    }

    registerChangeInConnections() {
        this.eventSubscriber = this.eventManager.subscribe('connectionListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
