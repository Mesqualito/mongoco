import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IConnection } from 'app/shared/model/mongoco/connection.model';

@Component({
    selector: 'jhi-connection-detail',
    templateUrl: './connection-detail.component.html'
})
export class ConnectionDetailComponent implements OnInit {
    connection: IConnection;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ connection }) => {
            this.connection = connection;
        });
    }

    previousState() {
        window.history.back();
    }
}
