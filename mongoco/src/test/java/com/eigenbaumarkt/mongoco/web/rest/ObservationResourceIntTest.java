package com.eigenbaumarkt.mongoco.web.rest;

import com.eigenbaumarkt.mongoco.MongocoApp;

import com.eigenbaumarkt.mongoco.config.SecurityBeanOverrideConfiguration;

import com.eigenbaumarkt.mongoco.domain.Observation;
import com.eigenbaumarkt.mongoco.repository.ObservationRepository;
import com.eigenbaumarkt.mongoco.repository.search.ObservationSearchRepository;
import com.eigenbaumarkt.mongoco.service.ObservationService;
import com.eigenbaumarkt.mongoco.service.dto.ObservationDTO;
import com.eigenbaumarkt.mongoco.service.mapper.ObservationMapper;
import com.eigenbaumarkt.mongoco.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.Validator;

import java.util.Collections;
import java.util.List;


import static com.eigenbaumarkt.mongoco.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ObservationResource REST controller.
 *
 * @see ObservationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SecurityBeanOverrideConfiguration.class, MongocoApp.class})
public class ObservationResourceIntTest {

    private static final Long DEFAULT_INT_ID = 1L;
    private static final Long UPDATED_INT_ID = 2L;

    private static final String DEFAULT_TERM = "AAAAAAAAAA";
    private static final String UPDATED_TERM = "BBBBBBBBBB";

    @Autowired
    private ObservationRepository observationRepository;

    @Autowired
    private ObservationMapper observationMapper;

    @Autowired
    private ObservationService observationService;

    /**
     * This repository is mocked in the com.eigenbaumarkt.mongoco.repository.search test package.
     *
     * @see com.eigenbaumarkt.mongoco.repository.search.ObservationSearchRepositoryMockConfiguration
     */
    @Autowired
    private ObservationSearchRepository mockObservationSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private Validator validator;

    private MockMvc restObservationMockMvc;

    private Observation observation;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ObservationResource observationResource = new ObservationResource(observationService);
        this.restObservationMockMvc = MockMvcBuilders.standaloneSetup(observationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Observation createEntity() {
        Observation observation = new Observation()
            .intID(DEFAULT_INT_ID)
            .term(DEFAULT_TERM);
        return observation;
    }

    @Before
    public void initTest() {
        observationRepository.deleteAll();
        observation = createEntity();
    }

    @Test
    public void createObservation() throws Exception {
        int databaseSizeBeforeCreate = observationRepository.findAll().size();

        // Create the Observation
        ObservationDTO observationDTO = observationMapper.toDto(observation);
        restObservationMockMvc.perform(post("/api/observations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(observationDTO)))
            .andExpect(status().isCreated());

        // Validate the Observation in the database
        List<Observation> observationList = observationRepository.findAll();
        assertThat(observationList).hasSize(databaseSizeBeforeCreate + 1);
        Observation testObservation = observationList.get(observationList.size() - 1);
        assertThat(testObservation.getIntID()).isEqualTo(DEFAULT_INT_ID);
        assertThat(testObservation.getTerm()).isEqualTo(DEFAULT_TERM);

        // Validate the Observation in Elasticsearch
        verify(mockObservationSearchRepository, times(1)).save(testObservation);
    }

    @Test
    public void createObservationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = observationRepository.findAll().size();

        // Create the Observation with an existing ID
        observation.setId("existing_id");
        ObservationDTO observationDTO = observationMapper.toDto(observation);

        // An entity with an existing ID cannot be created, so this API call must fail
        restObservationMockMvc.perform(post("/api/observations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(observationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Observation in the database
        List<Observation> observationList = observationRepository.findAll();
        assertThat(observationList).hasSize(databaseSizeBeforeCreate);

        // Validate the Observation in Elasticsearch
        verify(mockObservationSearchRepository, times(0)).save(observation);
    }

    @Test
    public void checkTermIsRequired() throws Exception {
        int databaseSizeBeforeTest = observationRepository.findAll().size();
        // set the field null
        observation.setTerm(null);

        // Create the Observation, which fails.
        ObservationDTO observationDTO = observationMapper.toDto(observation);

        restObservationMockMvc.perform(post("/api/observations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(observationDTO)))
            .andExpect(status().isBadRequest());

        List<Observation> observationList = observationRepository.findAll();
        assertThat(observationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllObservations() throws Exception {
        // Initialize the database
        observationRepository.save(observation);

        // Get all the observationList
        restObservationMockMvc.perform(get("/api/observations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(observation.getId())))
            .andExpect(jsonPath("$.[*].intID").value(hasItem(DEFAULT_INT_ID.intValue())))
            .andExpect(jsonPath("$.[*].term").value(hasItem(DEFAULT_TERM.toString())));
    }
    
    @Test
    public void getObservation() throws Exception {
        // Initialize the database
        observationRepository.save(observation);

        // Get the observation
        restObservationMockMvc.perform(get("/api/observations/{id}", observation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(observation.getId()))
            .andExpect(jsonPath("$.intID").value(DEFAULT_INT_ID.intValue()))
            .andExpect(jsonPath("$.term").value(DEFAULT_TERM.toString()));
    }

    @Test
    public void getNonExistingObservation() throws Exception {
        // Get the observation
        restObservationMockMvc.perform(get("/api/observations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateObservation() throws Exception {
        // Initialize the database
        observationRepository.save(observation);

        int databaseSizeBeforeUpdate = observationRepository.findAll().size();

        // Update the observation
        Observation updatedObservation = observationRepository.findById(observation.getId()).get();
        updatedObservation
            .intID(UPDATED_INT_ID)
            .term(UPDATED_TERM);
        ObservationDTO observationDTO = observationMapper.toDto(updatedObservation);

        restObservationMockMvc.perform(put("/api/observations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(observationDTO)))
            .andExpect(status().isOk());

        // Validate the Observation in the database
        List<Observation> observationList = observationRepository.findAll();
        assertThat(observationList).hasSize(databaseSizeBeforeUpdate);
        Observation testObservation = observationList.get(observationList.size() - 1);
        assertThat(testObservation.getIntID()).isEqualTo(UPDATED_INT_ID);
        assertThat(testObservation.getTerm()).isEqualTo(UPDATED_TERM);

        // Validate the Observation in Elasticsearch
        verify(mockObservationSearchRepository, times(1)).save(testObservation);
    }

    @Test
    public void updateNonExistingObservation() throws Exception {
        int databaseSizeBeforeUpdate = observationRepository.findAll().size();

        // Create the Observation
        ObservationDTO observationDTO = observationMapper.toDto(observation);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restObservationMockMvc.perform(put("/api/observations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(observationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Observation in the database
        List<Observation> observationList = observationRepository.findAll();
        assertThat(observationList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Observation in Elasticsearch
        verify(mockObservationSearchRepository, times(0)).save(observation);
    }

    @Test
    public void deleteObservation() throws Exception {
        // Initialize the database
        observationRepository.save(observation);

        int databaseSizeBeforeDelete = observationRepository.findAll().size();

        // Delete the observation
        restObservationMockMvc.perform(delete("/api/observations/{id}", observation.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Observation> observationList = observationRepository.findAll();
        assertThat(observationList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Observation in Elasticsearch
        verify(mockObservationSearchRepository, times(1)).deleteById(observation.getId());
    }

    @Test
    public void searchObservation() throws Exception {
        // Initialize the database
        observationRepository.save(observation);
        when(mockObservationSearchRepository.search(queryStringQuery("id:" + observation.getId())))
            .thenReturn(Collections.singletonList(observation));
        // Search the observation
        restObservationMockMvc.perform(get("/api/_search/observations?query=id:" + observation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(observation.getId())))
            .andExpect(jsonPath("$.[*].intID").value(hasItem(DEFAULT_INT_ID.intValue())))
            .andExpect(jsonPath("$.[*].term").value(hasItem(DEFAULT_TERM)));
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Observation.class);
        Observation observation1 = new Observation();
        observation1.setId("id1");
        Observation observation2 = new Observation();
        observation2.setId(observation1.getId());
        assertThat(observation1).isEqualTo(observation2);
        observation2.setId("id2");
        assertThat(observation1).isNotEqualTo(observation2);
        observation1.setId(null);
        assertThat(observation1).isNotEqualTo(observation2);
    }

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ObservationDTO.class);
        ObservationDTO observationDTO1 = new ObservationDTO();
        observationDTO1.setId("id1");
        ObservationDTO observationDTO2 = new ObservationDTO();
        assertThat(observationDTO1).isNotEqualTo(observationDTO2);
        observationDTO2.setId(observationDTO1.getId());
        assertThat(observationDTO1).isEqualTo(observationDTO2);
        observationDTO2.setId("id2");
        assertThat(observationDTO1).isNotEqualTo(observationDTO2);
        observationDTO1.setId(null);
        assertThat(observationDTO1).isNotEqualTo(observationDTO2);
    }
}
