/**
 * View Models used by Spring MVC REST controllers.
 */
package com.eigenbaumarkt.mongoco.web.rest.vm;
