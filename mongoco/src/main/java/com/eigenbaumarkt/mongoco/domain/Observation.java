package com.eigenbaumarkt.mongoco.domain;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DBRef;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Observation.
 */
@Document(collection = "observation")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "observation")
public class Observation implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    private String id;

    @Field("int_id")
    private Long intID;

    @NotNull
    @Field("term")
    private String term;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getIntID() {
        return intID;
    }

    public Observation intID(Long intID) {
        this.intID = intID;
        return this;
    }

    public void setIntID(Long intID) {
        this.intID = intID;
    }

    public String getTerm() {
        return term;
    }

    public Observation term(String term) {
        this.term = term;
        return this;
    }

    public void setTerm(String term) {
        this.term = term;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Observation observation = (Observation) o;
        if (observation.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), observation.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Observation{" +
            "id=" + getId() +
            ", intID=" + getIntID() +
            ", term='" + getTerm() + "'" +
            "}";
    }
}
