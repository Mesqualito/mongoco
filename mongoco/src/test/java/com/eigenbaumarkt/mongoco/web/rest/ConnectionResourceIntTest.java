package com.eigenbaumarkt.mongoco.web.rest;

import com.eigenbaumarkt.mongoco.MongocoApp;

import com.eigenbaumarkt.mongoco.config.SecurityBeanOverrideConfiguration;

import com.eigenbaumarkt.mongoco.domain.Connection;
import com.eigenbaumarkt.mongoco.repository.ConnectionRepository;
import com.eigenbaumarkt.mongoco.repository.search.ConnectionSearchRepository;
import com.eigenbaumarkt.mongoco.service.ConnectionService;
import com.eigenbaumarkt.mongoco.service.dto.ConnectionDTO;
import com.eigenbaumarkt.mongoco.service.mapper.ConnectionMapper;
import com.eigenbaumarkt.mongoco.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.Validator;

import java.util.Collections;
import java.util.List;


import static com.eigenbaumarkt.mongoco.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ConnectionResource REST controller.
 *
 * @see ConnectionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SecurityBeanOverrideConfiguration.class, MongocoApp.class})
public class ConnectionResourceIntTest {

    private static final Long DEFAULT_INT_ID = 1L;
    private static final Long UPDATED_INT_ID = 2L;

    private static final Long DEFAULT_ARTICLE_ID = 1L;
    private static final Long UPDATED_ARTICLE_ID = 2L;

    private static final Long DEFAULT_LANGUAGE_ID = 1L;
    private static final Long UPDATED_LANGUAGE_ID = 2L;

    private static final Long DEFAULT_OBSERVATION_ID = 1L;
    private static final Long UPDATED_OBSERVATION_ID = 2L;

    private static final String DEFAULT_TERM = "AAAAAAAAAA";
    private static final String UPDATED_TERM = "BBBBBBBBBB";

    @Autowired
    private ConnectionRepository connectionRepository;

    @Autowired
    private ConnectionMapper connectionMapper;

    @Autowired
    private ConnectionService connectionService;

    /**
     * This repository is mocked in the com.eigenbaumarkt.mongoco.repository.search test package.
     *
     * @see com.eigenbaumarkt.mongoco.repository.search.ConnectionSearchRepositoryMockConfiguration
     */
    @Autowired
    private ConnectionSearchRepository mockConnectionSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private Validator validator;

    private MockMvc restConnectionMockMvc;

    private Connection connection;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ConnectionResource connectionResource = new ConnectionResource(connectionService);
        this.restConnectionMockMvc = MockMvcBuilders.standaloneSetup(connectionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Connection createEntity() {
        Connection connection = new Connection()
            .intID(DEFAULT_INT_ID)
            .articleID(DEFAULT_ARTICLE_ID)
            .languageID(DEFAULT_LANGUAGE_ID)
            .observationID(DEFAULT_OBSERVATION_ID)
            .term(DEFAULT_TERM);
        return connection;
    }

    @Before
    public void initTest() {
        connectionRepository.deleteAll();
        connection = createEntity();
    }

    @Test
    public void createConnection() throws Exception {
        int databaseSizeBeforeCreate = connectionRepository.findAll().size();

        // Create the Connection
        ConnectionDTO connectionDTO = connectionMapper.toDto(connection);
        restConnectionMockMvc.perform(post("/api/connections")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(connectionDTO)))
            .andExpect(status().isCreated());

        // Validate the Connection in the database
        List<Connection> connectionList = connectionRepository.findAll();
        assertThat(connectionList).hasSize(databaseSizeBeforeCreate + 1);
        Connection testConnection = connectionList.get(connectionList.size() - 1);
        assertThat(testConnection.getIntID()).isEqualTo(DEFAULT_INT_ID);
        assertThat(testConnection.getArticleID()).isEqualTo(DEFAULT_ARTICLE_ID);
        assertThat(testConnection.getLanguageID()).isEqualTo(DEFAULT_LANGUAGE_ID);
        assertThat(testConnection.getObservationID()).isEqualTo(DEFAULT_OBSERVATION_ID);
        assertThat(testConnection.getTerm()).isEqualTo(DEFAULT_TERM);

        // Validate the Connection in Elasticsearch
        verify(mockConnectionSearchRepository, times(1)).save(testConnection);
    }

    @Test
    public void createConnectionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = connectionRepository.findAll().size();

        // Create the Connection with an existing ID
        connection.setId("existing_id");
        ConnectionDTO connectionDTO = connectionMapper.toDto(connection);

        // An entity with an existing ID cannot be created, so this API call must fail
        restConnectionMockMvc.perform(post("/api/connections")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(connectionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Connection in the database
        List<Connection> connectionList = connectionRepository.findAll();
        assertThat(connectionList).hasSize(databaseSizeBeforeCreate);

        // Validate the Connection in Elasticsearch
        verify(mockConnectionSearchRepository, times(0)).save(connection);
    }

    @Test
    public void checkTermIsRequired() throws Exception {
        int databaseSizeBeforeTest = connectionRepository.findAll().size();
        // set the field null
        connection.setTerm(null);

        // Create the Connection, which fails.
        ConnectionDTO connectionDTO = connectionMapper.toDto(connection);

        restConnectionMockMvc.perform(post("/api/connections")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(connectionDTO)))
            .andExpect(status().isBadRequest());

        List<Connection> connectionList = connectionRepository.findAll();
        assertThat(connectionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllConnections() throws Exception {
        // Initialize the database
        connectionRepository.save(connection);

        // Get all the connectionList
        restConnectionMockMvc.perform(get("/api/connections?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(connection.getId())))
            .andExpect(jsonPath("$.[*].intID").value(hasItem(DEFAULT_INT_ID.intValue())))
            .andExpect(jsonPath("$.[*].articleID").value(hasItem(DEFAULT_ARTICLE_ID.intValue())))
            .andExpect(jsonPath("$.[*].languageID").value(hasItem(DEFAULT_LANGUAGE_ID.intValue())))
            .andExpect(jsonPath("$.[*].observationID").value(hasItem(DEFAULT_OBSERVATION_ID.intValue())))
            .andExpect(jsonPath("$.[*].term").value(hasItem(DEFAULT_TERM.toString())));
    }
    
    @Test
    public void getConnection() throws Exception {
        // Initialize the database
        connectionRepository.save(connection);

        // Get the connection
        restConnectionMockMvc.perform(get("/api/connections/{id}", connection.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(connection.getId()))
            .andExpect(jsonPath("$.intID").value(DEFAULT_INT_ID.intValue()))
            .andExpect(jsonPath("$.articleID").value(DEFAULT_ARTICLE_ID.intValue()))
            .andExpect(jsonPath("$.languageID").value(DEFAULT_LANGUAGE_ID.intValue()))
            .andExpect(jsonPath("$.observationID").value(DEFAULT_OBSERVATION_ID.intValue()))
            .andExpect(jsonPath("$.term").value(DEFAULT_TERM.toString()));
    }

    @Test
    public void getNonExistingConnection() throws Exception {
        // Get the connection
        restConnectionMockMvc.perform(get("/api/connections/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateConnection() throws Exception {
        // Initialize the database
        connectionRepository.save(connection);

        int databaseSizeBeforeUpdate = connectionRepository.findAll().size();

        // Update the connection
        Connection updatedConnection = connectionRepository.findById(connection.getId()).get();
        updatedConnection
            .intID(UPDATED_INT_ID)
            .articleID(UPDATED_ARTICLE_ID)
            .languageID(UPDATED_LANGUAGE_ID)
            .observationID(UPDATED_OBSERVATION_ID)
            .term(UPDATED_TERM);
        ConnectionDTO connectionDTO = connectionMapper.toDto(updatedConnection);

        restConnectionMockMvc.perform(put("/api/connections")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(connectionDTO)))
            .andExpect(status().isOk());

        // Validate the Connection in the database
        List<Connection> connectionList = connectionRepository.findAll();
        assertThat(connectionList).hasSize(databaseSizeBeforeUpdate);
        Connection testConnection = connectionList.get(connectionList.size() - 1);
        assertThat(testConnection.getIntID()).isEqualTo(UPDATED_INT_ID);
        assertThat(testConnection.getArticleID()).isEqualTo(UPDATED_ARTICLE_ID);
        assertThat(testConnection.getLanguageID()).isEqualTo(UPDATED_LANGUAGE_ID);
        assertThat(testConnection.getObservationID()).isEqualTo(UPDATED_OBSERVATION_ID);
        assertThat(testConnection.getTerm()).isEqualTo(UPDATED_TERM);

        // Validate the Connection in Elasticsearch
        verify(mockConnectionSearchRepository, times(1)).save(testConnection);
    }

    @Test
    public void updateNonExistingConnection() throws Exception {
        int databaseSizeBeforeUpdate = connectionRepository.findAll().size();

        // Create the Connection
        ConnectionDTO connectionDTO = connectionMapper.toDto(connection);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restConnectionMockMvc.perform(put("/api/connections")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(connectionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Connection in the database
        List<Connection> connectionList = connectionRepository.findAll();
        assertThat(connectionList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Connection in Elasticsearch
        verify(mockConnectionSearchRepository, times(0)).save(connection);
    }

    @Test
    public void deleteConnection() throws Exception {
        // Initialize the database
        connectionRepository.save(connection);

        int databaseSizeBeforeDelete = connectionRepository.findAll().size();

        // Delete the connection
        restConnectionMockMvc.perform(delete("/api/connections/{id}", connection.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Connection> connectionList = connectionRepository.findAll();
        assertThat(connectionList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Connection in Elasticsearch
        verify(mockConnectionSearchRepository, times(1)).deleteById(connection.getId());
    }

    @Test
    public void searchConnection() throws Exception {
        // Initialize the database
        connectionRepository.save(connection);
        when(mockConnectionSearchRepository.search(queryStringQuery("id:" + connection.getId())))
            .thenReturn(Collections.singletonList(connection));
        // Search the connection
        restConnectionMockMvc.perform(get("/api/_search/connections?query=id:" + connection.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(connection.getId())))
            .andExpect(jsonPath("$.[*].intID").value(hasItem(DEFAULT_INT_ID.intValue())))
            .andExpect(jsonPath("$.[*].articleID").value(hasItem(DEFAULT_ARTICLE_ID.intValue())))
            .andExpect(jsonPath("$.[*].languageID").value(hasItem(DEFAULT_LANGUAGE_ID.intValue())))
            .andExpect(jsonPath("$.[*].observationID").value(hasItem(DEFAULT_OBSERVATION_ID.intValue())))
            .andExpect(jsonPath("$.[*].term").value(hasItem(DEFAULT_TERM)));
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Connection.class);
        Connection connection1 = new Connection();
        connection1.setId("id1");
        Connection connection2 = new Connection();
        connection2.setId(connection1.getId());
        assertThat(connection1).isEqualTo(connection2);
        connection2.setId("id2");
        assertThat(connection1).isNotEqualTo(connection2);
        connection1.setId(null);
        assertThat(connection1).isNotEqualTo(connection2);
    }

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ConnectionDTO.class);
        ConnectionDTO connectionDTO1 = new ConnectionDTO();
        connectionDTO1.setId("id1");
        ConnectionDTO connectionDTO2 = new ConnectionDTO();
        assertThat(connectionDTO1).isNotEqualTo(connectionDTO2);
        connectionDTO2.setId(connectionDTO1.getId());
        assertThat(connectionDTO1).isEqualTo(connectionDTO2);
        connectionDTO2.setId("id2");
        assertThat(connectionDTO1).isNotEqualTo(connectionDTO2);
        connectionDTO1.setId(null);
        assertThat(connectionDTO1).isNotEqualTo(connectionDTO2);
    }
}
