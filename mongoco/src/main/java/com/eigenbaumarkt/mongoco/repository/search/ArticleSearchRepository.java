package com.eigenbaumarkt.mongoco.repository.search;

import com.eigenbaumarkt.mongoco.domain.Article;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Article entity.
 */
public interface ArticleSearchRepository extends ElasticsearchRepository<Article, String> {
}
