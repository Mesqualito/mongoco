package com.eigenbaumarkt.mongoco.repository.search;

import com.eigenbaumarkt.mongoco.domain.Connection;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Connection entity.
 */
public interface ConnectionSearchRepository extends ElasticsearchRepository<Connection, String> {
}
