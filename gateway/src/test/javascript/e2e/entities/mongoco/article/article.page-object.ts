import { element, by, ElementFinder } from 'protractor';

export class ArticleComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-article div table .btn-danger'));
    title = element.all(by.css('jhi-article div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class ArticleUpdatePage {
    pageTitle = element(by.id('jhi-article-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    intIDInput = element(by.id('field_intID'));
    languageIDInput = element(by.id('field_languageID'));
    connectionIDInput = element(by.id('field_connectionID'));
    observationIDInput = element(by.id('field_observationID'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setIntIDInput(intID) {
        await this.intIDInput.sendKeys(intID);
    }

    async getIntIDInput() {
        return this.intIDInput.getAttribute('value');
    }

    async setLanguageIDInput(languageID) {
        await this.languageIDInput.sendKeys(languageID);
    }

    async getLanguageIDInput() {
        return this.languageIDInput.getAttribute('value');
    }

    async setConnectionIDInput(connectionID) {
        await this.connectionIDInput.sendKeys(connectionID);
    }

    async getConnectionIDInput() {
        return this.connectionIDInput.getAttribute('value');
    }

    async setObservationIDInput(observationID) {
        await this.observationIDInput.sendKeys(observationID);
    }

    async getObservationIDInput() {
        return this.observationIDInput.getAttribute('value');
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class ArticleDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-article-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-article'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
