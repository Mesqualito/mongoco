export interface ILanguage {
    id?: string;
    intID?: number;
    code?: string;
    description?: string;
}

export class Language implements ILanguage {
    constructor(public id?: string, public intID?: number, public code?: string, public description?: string) {}
}
