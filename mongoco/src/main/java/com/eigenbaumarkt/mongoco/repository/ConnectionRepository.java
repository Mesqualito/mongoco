package com.eigenbaumarkt.mongoco.repository;

import com.eigenbaumarkt.mongoco.domain.Connection;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data MongoDB repository for the Connection entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ConnectionRepository extends MongoRepository<Connection, String> {

}
