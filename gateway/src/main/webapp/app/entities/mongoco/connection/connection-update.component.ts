import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { IConnection } from 'app/shared/model/mongoco/connection.model';
import { ConnectionService } from './connection.service';

@Component({
    selector: 'jhi-connection-update',
    templateUrl: './connection-update.component.html'
})
export class ConnectionUpdateComponent implements OnInit {
    connection: IConnection;
    isSaving: boolean;

    constructor(protected connectionService: ConnectionService, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ connection }) => {
            this.connection = connection;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.connection.id !== undefined) {
            this.subscribeToSaveResponse(this.connectionService.update(this.connection));
        } else {
            this.subscribeToSaveResponse(this.connectionService.create(this.connection));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IConnection>>) {
        result.subscribe((res: HttpResponse<IConnection>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}
