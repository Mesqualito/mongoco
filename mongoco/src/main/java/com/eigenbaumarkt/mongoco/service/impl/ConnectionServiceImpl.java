package com.eigenbaumarkt.mongoco.service.impl;

import com.eigenbaumarkt.mongoco.service.ConnectionService;
import com.eigenbaumarkt.mongoco.domain.Connection;
import com.eigenbaumarkt.mongoco.repository.ConnectionRepository;
import com.eigenbaumarkt.mongoco.repository.search.ConnectionSearchRepository;
import com.eigenbaumarkt.mongoco.service.dto.ConnectionDTO;
import com.eigenbaumarkt.mongoco.service.mapper.ConnectionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Connection.
 */
@Service
public class ConnectionServiceImpl implements ConnectionService {

    private final Logger log = LoggerFactory.getLogger(ConnectionServiceImpl.class);

    private final ConnectionRepository connectionRepository;

    private final ConnectionMapper connectionMapper;

    private final ConnectionSearchRepository connectionSearchRepository;

    public ConnectionServiceImpl(ConnectionRepository connectionRepository, ConnectionMapper connectionMapper, ConnectionSearchRepository connectionSearchRepository) {
        this.connectionRepository = connectionRepository;
        this.connectionMapper = connectionMapper;
        this.connectionSearchRepository = connectionSearchRepository;
    }

    /**
     * Save a connection.
     *
     * @param connectionDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ConnectionDTO save(ConnectionDTO connectionDTO) {
        log.debug("Request to save Connection : {}", connectionDTO);
        Connection connection = connectionMapper.toEntity(connectionDTO);
        connection = connectionRepository.save(connection);
        ConnectionDTO result = connectionMapper.toDto(connection);
        connectionSearchRepository.save(connection);
        return result;
    }

    /**
     * Get all the connections.
     *
     * @return the list of entities
     */
    @Override
    public List<ConnectionDTO> findAll() {
        log.debug("Request to get all Connections");
        return connectionRepository.findAll().stream()
            .map(connectionMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one connection by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public Optional<ConnectionDTO> findOne(String id) {
        log.debug("Request to get Connection : {}", id);
        return connectionRepository.findById(id)
            .map(connectionMapper::toDto);
    }

    /**
     * Delete the connection by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Connection : {}", id);        connectionRepository.deleteById(id);
        connectionSearchRepository.deleteById(id);
    }

    /**
     * Search for the connection corresponding to the query.
     *
     * @param query the query of the search
     * @return the list of entities
     */
    @Override
    public List<ConnectionDTO> search(String query) {
        log.debug("Request to search Connections for query {}", query);
        return StreamSupport
            .stream(connectionSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(connectionMapper::toDto)
            .collect(Collectors.toList());
    }
}
