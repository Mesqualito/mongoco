import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Connection } from 'app/shared/model/mongoco/connection.model';
import { ConnectionService } from './connection.service';
import { ConnectionComponent } from './connection.component';
import { ConnectionDetailComponent } from './connection-detail.component';
import { ConnectionUpdateComponent } from './connection-update.component';
import { ConnectionDeletePopupComponent } from './connection-delete-dialog.component';
import { IConnection } from 'app/shared/model/mongoco/connection.model';

@Injectable({ providedIn: 'root' })
export class ConnectionResolve implements Resolve<IConnection> {
    constructor(private service: ConnectionService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IConnection> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Connection>) => response.ok),
                map((connection: HttpResponse<Connection>) => connection.body)
            );
        }
        return of(new Connection());
    }
}

export const connectionRoute: Routes = [
    {
        path: '',
        component: ConnectionComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.mongocoConnection.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: ConnectionDetailComponent,
        resolve: {
            connection: ConnectionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.mongocoConnection.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: ConnectionUpdateComponent,
        resolve: {
            connection: ConnectionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.mongocoConnection.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: ConnectionUpdateComponent,
        resolve: {
            connection: ConnectionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.mongocoConnection.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const connectionPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: ConnectionDeletePopupComponent,
        resolve: {
            connection: ConnectionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.mongocoConnection.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
