/**
 * View Models used by Spring MVC REST controllers.
 */
package com.eigenbaumarkt.uaa.web.rest.vm;
