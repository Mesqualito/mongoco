/**
 * View Models used by Spring MVC REST controllers.
 */
package com.eigenbaumarkt.gateway.web.rest.vm;
