package com.eigenbaumarkt.mongoco.repository;

import com.eigenbaumarkt.mongoco.domain.Observation;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data MongoDB repository for the Observation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ObservationRepository extends MongoRepository<Observation, String> {

}
