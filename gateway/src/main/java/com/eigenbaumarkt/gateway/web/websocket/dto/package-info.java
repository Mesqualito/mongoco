/**
 * Data Access Objects used by WebSocket services.
 */
package com.eigenbaumarkt.gateway.web.websocket.dto;
