export interface IObservation {
    id?: string;
    intID?: number;
    term?: string;
}

export class Observation implements IObservation {
    constructor(public id?: string, public intID?: number, public term?: string) {}
}
