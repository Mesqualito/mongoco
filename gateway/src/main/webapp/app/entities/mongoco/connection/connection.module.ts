import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { GatewaySharedModule } from 'app/shared';
import {
    ConnectionComponent,
    ConnectionDetailComponent,
    ConnectionUpdateComponent,
    ConnectionDeletePopupComponent,
    ConnectionDeleteDialogComponent,
    connectionRoute,
    connectionPopupRoute
} from './';

const ENTITY_STATES = [...connectionRoute, ...connectionPopupRoute];

@NgModule({
    imports: [GatewaySharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ConnectionComponent,
        ConnectionDetailComponent,
        ConnectionUpdateComponent,
        ConnectionDeleteDialogComponent,
        ConnectionDeletePopupComponent
    ],
    entryComponents: [ConnectionComponent, ConnectionUpdateComponent, ConnectionDeleteDialogComponent, ConnectionDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MongocoConnectionModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
