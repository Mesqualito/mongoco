/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../../page-objects/jhi-page-objects';

import { ArticleComponentsPage, ArticleDeleteDialog, ArticleUpdatePage } from './article.page-object';

const expect = chai.expect;

describe('Article e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let articleUpdatePage: ArticleUpdatePage;
    let articleComponentsPage: ArticleComponentsPage;
    let articleDeleteDialog: ArticleDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Articles', async () => {
        await navBarPage.goToEntity('article');
        articleComponentsPage = new ArticleComponentsPage();
        await browser.wait(ec.visibilityOf(articleComponentsPage.title), 5000);
        expect(await articleComponentsPage.getTitle()).to.eq('gatewayApp.mongocoArticle.home.title');
    });

    it('should load create Article page', async () => {
        await articleComponentsPage.clickOnCreateButton();
        articleUpdatePage = new ArticleUpdatePage();
        expect(await articleUpdatePage.getPageTitle()).to.eq('gatewayApp.mongocoArticle.home.createOrEditLabel');
        await articleUpdatePage.cancel();
    });

    it('should create and save Articles', async () => {
        const nbButtonsBeforeCreate = await articleComponentsPage.countDeleteButtons();

        await articleComponentsPage.clickOnCreateButton();
        await promise.all([
            articleUpdatePage.setIntIDInput('5'),
            articleUpdatePage.setLanguageIDInput('5'),
            articleUpdatePage.setConnectionIDInput('5'),
            articleUpdatePage.setObservationIDInput('5')
        ]);
        expect(await articleUpdatePage.getIntIDInput()).to.eq('5');
        expect(await articleUpdatePage.getLanguageIDInput()).to.eq('5');
        expect(await articleUpdatePage.getConnectionIDInput()).to.eq('5');
        expect(await articleUpdatePage.getObservationIDInput()).to.eq('5');
        await articleUpdatePage.save();
        expect(await articleUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await articleComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Article', async () => {
        const nbButtonsBeforeDelete = await articleComponentsPage.countDeleteButtons();
        await articleComponentsPage.clickOnLastDeleteButton();

        articleDeleteDialog = new ArticleDeleteDialog();
        expect(await articleDeleteDialog.getDialogTitle()).to.eq('gatewayApp.mongocoArticle.delete.question');
        await articleDeleteDialog.clickOnConfirmButton();

        expect(await articleComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
