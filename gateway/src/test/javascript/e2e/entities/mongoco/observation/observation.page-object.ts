import { element, by, ElementFinder } from 'protractor';

export class ObservationComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-observation div table .btn-danger'));
    title = element.all(by.css('jhi-observation div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class ObservationUpdatePage {
    pageTitle = element(by.id('jhi-observation-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    intIDInput = element(by.id('field_intID'));
    termInput = element(by.id('field_term'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setIntIDInput(intID) {
        await this.intIDInput.sendKeys(intID);
    }

    async getIntIDInput() {
        return this.intIDInput.getAttribute('value');
    }

    async setTermInput(term) {
        await this.termInput.sendKeys(term);
    }

    async getTermInput() {
        return this.termInput.getAttribute('value');
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class ObservationDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-observation-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-observation'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
