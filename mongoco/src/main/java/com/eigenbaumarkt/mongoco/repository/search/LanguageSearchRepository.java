package com.eigenbaumarkt.mongoco.repository.search;

import com.eigenbaumarkt.mongoco.domain.Language;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Language entity.
 */
public interface LanguageSearchRepository extends ElasticsearchRepository<Language, String> {
}
