package com.eigenbaumarkt.mongoco.domain;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DBRef;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Connection.
 */
@Document(collection = "translation")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "connection")
public class Connection implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    private String id;

    @Field("int_id")
    private Long intID;

    @Field("article_id")
    private Long articleID;

    @Field("language_id")
    private Long languageID;

    @Field("observation_id")
    private Long observationID;

    @NotNull
    @Field("term")
    private String term;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getIntID() {
        return intID;
    }

    public Connection intID(Long intID) {
        this.intID = intID;
        return this;
    }

    public void setIntID(Long intID) {
        this.intID = intID;
    }

    public Long getArticleID() {
        return articleID;
    }

    public Connection articleID(Long articleID) {
        this.articleID = articleID;
        return this;
    }

    public void setArticleID(Long articleID) {
        this.articleID = articleID;
    }

    public Long getLanguageID() {
        return languageID;
    }

    public Connection languageID(Long languageID) {
        this.languageID = languageID;
        return this;
    }

    public void setLanguageID(Long languageID) {
        this.languageID = languageID;
    }

    public Long getObservationID() {
        return observationID;
    }

    public Connection observationID(Long observationID) {
        this.observationID = observationID;
        return this;
    }

    public void setObservationID(Long observationID) {
        this.observationID = observationID;
    }

    public String getTerm() {
        return term;
    }

    public Connection term(String term) {
        this.term = term;
        return this;
    }

    public void setTerm(String term) {
        this.term = term;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Connection connection = (Connection) o;
        if (connection.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), connection.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Connection{" +
            "id=" + getId() +
            ", intID=" + getIntID() +
            ", articleID=" + getArticleID() +
            ", languageID=" + getLanguageID() +
            ", observationID=" + getObservationID() +
            ", term='" + getTerm() + "'" +
            "}";
    }
}
