package com.eigenbaumarkt.mongoco.repository.search;

import com.eigenbaumarkt.mongoco.domain.Observation;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Observation entity.
 */
public interface ObservationSearchRepository extends ElasticsearchRepository<Observation, String> {
}
