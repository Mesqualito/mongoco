import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'article',
                loadChildren: './mongoco/article/article.module#MongocoArticleModule'
            },
            {
                path: 'connection',
                loadChildren: './mongoco/connection/connection.module#MongocoConnectionModule'
            },
            {
                path: 'language',
                loadChildren: './mongoco/language/language.module#MongocoLanguageModule'
            },
            {
                path: 'observation',
                loadChildren: './mongoco/observation/observation.module#MongocoObservationModule'
            }
            /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
        ])
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayEntityModule {}
