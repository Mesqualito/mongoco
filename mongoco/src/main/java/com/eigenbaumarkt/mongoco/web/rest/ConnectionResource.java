package com.eigenbaumarkt.mongoco.web.rest;
import com.eigenbaumarkt.mongoco.service.ConnectionService;
import com.eigenbaumarkt.mongoco.web.rest.errors.BadRequestAlertException;
import com.eigenbaumarkt.mongoco.web.rest.util.HeaderUtil;
import com.eigenbaumarkt.mongoco.service.dto.ConnectionDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Connection.
 */
@RestController
@RequestMapping("/api")
public class ConnectionResource {

    private final Logger log = LoggerFactory.getLogger(ConnectionResource.class);

    private static final String ENTITY_NAME = "mongocoConnection";

    private final ConnectionService connectionService;

    public ConnectionResource(ConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    /**
     * POST  /connections : Create a new connection.
     *
     * @param connectionDTO the connectionDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new connectionDTO, or with status 400 (Bad Request) if the connection has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/connections")
    public ResponseEntity<ConnectionDTO> createConnection(@Valid @RequestBody ConnectionDTO connectionDTO) throws URISyntaxException {
        log.debug("REST request to save Connection : {}", connectionDTO);
        if (connectionDTO.getId() != null) {
            throw new BadRequestAlertException("A new connection cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ConnectionDTO result = connectionService.save(connectionDTO);
        return ResponseEntity.created(new URI("/api/connections/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /connections : Updates an existing connection.
     *
     * @param connectionDTO the connectionDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated connectionDTO,
     * or with status 400 (Bad Request) if the connectionDTO is not valid,
     * or with status 500 (Internal Server Error) if the connectionDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/connections")
    public ResponseEntity<ConnectionDTO> updateConnection(@Valid @RequestBody ConnectionDTO connectionDTO) throws URISyntaxException {
        log.debug("REST request to update Connection : {}", connectionDTO);
        if (connectionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ConnectionDTO result = connectionService.save(connectionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, connectionDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /connections : get all the connections.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of connections in body
     */
    @GetMapping("/connections")
    public List<ConnectionDTO> getAllConnections() {
        log.debug("REST request to get all Connections");
        return connectionService.findAll();
    }

    /**
     * GET  /connections/:id : get the "id" connection.
     *
     * @param id the id of the connectionDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the connectionDTO, or with status 404 (Not Found)
     */
    @GetMapping("/connections/{id}")
    public ResponseEntity<ConnectionDTO> getConnection(@PathVariable String id) {
        log.debug("REST request to get Connection : {}", id);
        Optional<ConnectionDTO> connectionDTO = connectionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(connectionDTO);
    }

    /**
     * DELETE  /connections/:id : delete the "id" connection.
     *
     * @param id the id of the connectionDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/connections/{id}")
    public ResponseEntity<Void> deleteConnection(@PathVariable String id) {
        log.debug("REST request to delete Connection : {}", id);
        connectionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * SEARCH  /_search/connections?query=:query : search for the connection corresponding
     * to the query.
     *
     * @param query the query of the connection search
     * @return the result of the search
     */
    @GetMapping("/_search/connections")
    public List<ConnectionDTO> searchConnections(@RequestParam String query) {
        log.debug("REST request to search Connections for query {}", query);
        return connectionService.search(query);
    }

}
