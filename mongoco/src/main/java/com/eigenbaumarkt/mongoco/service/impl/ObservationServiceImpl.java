package com.eigenbaumarkt.mongoco.service.impl;

import com.eigenbaumarkt.mongoco.service.ObservationService;
import com.eigenbaumarkt.mongoco.domain.Observation;
import com.eigenbaumarkt.mongoco.repository.ObservationRepository;
import com.eigenbaumarkt.mongoco.repository.search.ObservationSearchRepository;
import com.eigenbaumarkt.mongoco.service.dto.ObservationDTO;
import com.eigenbaumarkt.mongoco.service.mapper.ObservationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Observation.
 */
@Service
public class ObservationServiceImpl implements ObservationService {

    private final Logger log = LoggerFactory.getLogger(ObservationServiceImpl.class);

    private final ObservationRepository observationRepository;

    private final ObservationMapper observationMapper;

    private final ObservationSearchRepository observationSearchRepository;

    public ObservationServiceImpl(ObservationRepository observationRepository, ObservationMapper observationMapper, ObservationSearchRepository observationSearchRepository) {
        this.observationRepository = observationRepository;
        this.observationMapper = observationMapper;
        this.observationSearchRepository = observationSearchRepository;
    }

    /**
     * Save a observation.
     *
     * @param observationDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ObservationDTO save(ObservationDTO observationDTO) {
        log.debug("Request to save Observation : {}", observationDTO);
        Observation observation = observationMapper.toEntity(observationDTO);
        observation = observationRepository.save(observation);
        ObservationDTO result = observationMapper.toDto(observation);
        observationSearchRepository.save(observation);
        return result;
    }

    /**
     * Get all the observations.
     *
     * @return the list of entities
     */
    @Override
    public List<ObservationDTO> findAll() {
        log.debug("Request to get all Observations");
        return observationRepository.findAll().stream()
            .map(observationMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one observation by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public Optional<ObservationDTO> findOne(String id) {
        log.debug("Request to get Observation : {}", id);
        return observationRepository.findById(id)
            .map(observationMapper::toDto);
    }

    /**
     * Delete the observation by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Observation : {}", id);        observationRepository.deleteById(id);
        observationSearchRepository.deleteById(id);
    }

    /**
     * Search for the observation corresponding to the query.
     *
     * @param query the query of the search
     * @return the list of entities
     */
    @Override
    public List<ObservationDTO> search(String query) {
        log.debug("Request to search Observations for query {}", query);
        return StreamSupport
            .stream(observationSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(observationMapper::toDto)
            .collect(Collectors.toList());
    }
}
