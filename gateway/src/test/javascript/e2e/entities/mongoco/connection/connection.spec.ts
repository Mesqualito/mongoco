/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../../page-objects/jhi-page-objects';

import { ConnectionComponentsPage, ConnectionDeleteDialog, ConnectionUpdatePage } from './connection.page-object';

const expect = chai.expect;

describe('Connection e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let connectionUpdatePage: ConnectionUpdatePage;
    let connectionComponentsPage: ConnectionComponentsPage;
    let connectionDeleteDialog: ConnectionDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Connections', async () => {
        await navBarPage.goToEntity('connection');
        connectionComponentsPage = new ConnectionComponentsPage();
        await browser.wait(ec.visibilityOf(connectionComponentsPage.title), 5000);
        expect(await connectionComponentsPage.getTitle()).to.eq('gatewayApp.mongocoConnection.home.title');
    });

    it('should load create Connection page', async () => {
        await connectionComponentsPage.clickOnCreateButton();
        connectionUpdatePage = new ConnectionUpdatePage();
        expect(await connectionUpdatePage.getPageTitle()).to.eq('gatewayApp.mongocoConnection.home.createOrEditLabel');
        await connectionUpdatePage.cancel();
    });

    it('should create and save Connections', async () => {
        const nbButtonsBeforeCreate = await connectionComponentsPage.countDeleteButtons();

        await connectionComponentsPage.clickOnCreateButton();
        await promise.all([
            connectionUpdatePage.setIntIDInput('5'),
            connectionUpdatePage.setArticleIDInput('5'),
            connectionUpdatePage.setLanguageIDInput('5'),
            connectionUpdatePage.setObservationIDInput('5'),
            connectionUpdatePage.setTermInput('term')
        ]);
        expect(await connectionUpdatePage.getIntIDInput()).to.eq('5');
        expect(await connectionUpdatePage.getArticleIDInput()).to.eq('5');
        expect(await connectionUpdatePage.getLanguageIDInput()).to.eq('5');
        expect(await connectionUpdatePage.getObservationIDInput()).to.eq('5');
        expect(await connectionUpdatePage.getTermInput()).to.eq('term');
        await connectionUpdatePage.save();
        expect(await connectionUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await connectionComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Connection', async () => {
        const nbButtonsBeforeDelete = await connectionComponentsPage.countDeleteButtons();
        await connectionComponentsPage.clickOnLastDeleteButton();

        connectionDeleteDialog = new ConnectionDeleteDialog();
        expect(await connectionDeleteDialog.getDialogTitle()).to.eq('gatewayApp.mongocoConnection.delete.question');
        await connectionDeleteDialog.clickOnConfirmButton();

        expect(await connectionComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
