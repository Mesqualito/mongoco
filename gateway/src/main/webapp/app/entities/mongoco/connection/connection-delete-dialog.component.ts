import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IConnection } from 'app/shared/model/mongoco/connection.model';
import { ConnectionService } from './connection.service';

@Component({
    selector: 'jhi-connection-delete-dialog',
    templateUrl: './connection-delete-dialog.component.html'
})
export class ConnectionDeleteDialogComponent {
    connection: IConnection;

    constructor(
        protected connectionService: ConnectionService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.connectionService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'connectionListModification',
                content: 'Deleted an connection'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-connection-delete-popup',
    template: ''
})
export class ConnectionDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ connection }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(ConnectionDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.connection = connection;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/connection', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/connection', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
