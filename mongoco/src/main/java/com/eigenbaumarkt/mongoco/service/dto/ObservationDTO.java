package com.eigenbaumarkt.mongoco.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Observation entity.
 */
public class ObservationDTO implements Serializable {

    private String id;

    private Long intID;

    @NotNull
    private String term;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getIntID() {
        return intID;
    }

    public void setIntID(Long intID) {
        this.intID = intID;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ObservationDTO observationDTO = (ObservationDTO) o;
        if (observationDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), observationDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ObservationDTO{" +
            "id=" + getId() +
            ", intID=" + getIntID() +
            ", term='" + getTerm() + "'" +
            "}";
    }
}
