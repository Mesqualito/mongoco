import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { IObservation } from 'app/shared/model/mongoco/observation.model';
import { ObservationService } from './observation.service';

@Component({
    selector: 'jhi-observation-update',
    templateUrl: './observation-update.component.html'
})
export class ObservationUpdateComponent implements OnInit {
    observation: IObservation;
    isSaving: boolean;

    constructor(protected observationService: ObservationService, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ observation }) => {
            this.observation = observation;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.observation.id !== undefined) {
            this.subscribeToSaveResponse(this.observationService.update(this.observation));
        } else {
            this.subscribeToSaveResponse(this.observationService.create(this.observation));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IObservation>>) {
        result.subscribe((res: HttpResponse<IObservation>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}
